Source: policykit-1
Section: admin
Priority: optional
Maintainer: Utopia Maintenance Team <pkg-utopia-maintainers@lists.alioth.debian.org>
Uploaders:
 Michael Biebl <biebl@debian.org>,
 Martin Pitt <mpitt@debian.org>,
 Simon McVittie <smcv@debian.org>,
 Luca Boccassi <bluca@debian.org>,
Build-Depends:
 dbus-daemon <!nocheck>,
 debhelper (>= 13.11.6~),
 debhelper-compat (= 13),
 dh-package-notes,
 dh-sequence-gir,
 docbook-xsl,
 duktape-dev,
 gir1.2-gio-2.0-dev,
 gobject-introspection (>= 1.78.1-9~),
 intltool,
 libexpat1-dev,
 libglib2.0-dev,
 libpam0g-dev,
 libselinux1-dev [linux-any],
 libsystemd-dev [linux-any],
 systemd-dev [linux-any],
 meson (>= 0.50.0),
 pkgconf,
 xml-core,
 xsltproc,
 python3-dbusmock <!nocheck>,
Build-Depends-Indep:
 gtk-doc-tools <!nodoc>,
 libglib2.0-doc <!nodoc>,
 libgtk-3-doc <!nodoc>,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/utopia-team/polkit.git
Vcs-Browser: https://salsa.debian.org/utopia-team/polkit
Homepage: https://github.com/polkit-org/polkit/

Package: polkitd
Architecture: linux-any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 adduser | systemd-sysusers,
 default-dbus-system-bus | dbus-system-bus,
 default-logind | logind,
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 policykit-1 (<< 0.120-4~),
 polkitd-javascript (<< 121+compat0.1-3~),
Replaces:
 policykit-1 (<< 0.120-4~),
 polkitd-javascript (<< 121+compat0.1-3~),
Multi-Arch: foreign
Description: framework for managing administrative policies and privileges
 polkit is an application-level toolkit for defining and handling the policy
 that allows unprivileged processes to speak to privileged processes.
 It was previously named PolicyKit.
 .
 It is a framework for centralizing the decision making process with respect to
 granting access to privileged operations for unprivileged (desktop)
 applications.
 .
 In a typical use of polkit, an unprivileged application such as gnome-disks
 sends requests via D-Bus or other inter-process communication mechanisms
 to a privileged system service such as udisks, which asks polkitd for
 permission to process those requests. This allows the application to carry
 out privileged tasks without making use of setuid, which avoids several
 common sources of security vulnerabilities.
 .
 This package provides the polkitd D-Bus service and supporting programs.
 The pkexec program is not included, and can be found in the pkexec package.

Package: pkexec
Architecture: linux-any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 polkitd (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 policykit-1 (<< 0.120-4~),
Replaces:
 policykit-1 (<< 0.120-4~),
Multi-Arch: foreign
Description: run commands as another user with polkit authorization
 polkit is an application-level toolkit for defining and handling the policy
 that allows unprivileged processes to speak to privileged processes.
 It was previously named PolicyKit.
 .
 pkexec is a setuid program to allow certain users to run commands as
 root or as a different user, similar to sudo. Unlike sudo, it carries
 out authentication and authorization by sending a request to polkit,
 so it uses desktop environments' familiar prompting mechanisms for
 authentication and uses polkit policies for authorization decisions.
 .
 By default, members of the 'sudo' Unix group can use pkexec to run any
 command after authenticating. The authorization rules can be changed by
 the local system administrator.
 .
 If this functionality is not required, removing the pkexec package will
 reduce security risk by removing a setuid program.

Package: policykit-1-doc
Build-Profiles: <!nodoc>
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends},
Suggests:
 devhelp,
Description: documentation for polkit
 polkit is a toolkit for defining and handling the policy that
 allows unprivileged processes to speak to privileged processes.
 It was previously named PolicyKit.
 .
 This package contains the API documentation of polkit.

Package: libpolkit-gobject-1-0
Architecture: any
Section: libs
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Multi-Arch: same
Description: polkit Authorization API
 polkit is a toolkit for defining and handling the policy that
 allows unprivileged processes to speak to privileged processes.
 It was previously named PolicyKit.
 .
 This package contains a library for accessing polkit.

Package: libpolkit-gobject-1-dev
Architecture: any
Section: libdevel
Depends:
 gir1.2-polkit-1.0 (= ${binary:Version}),
 libglib2.0-dev,
 libpolkit-gobject-1-0 (= ${binary:Version}),
 ${gir:Depends},
 ${misc:Depends},
Provides:
 ${gir:Provides},
Breaks:
 polkitd (<< 122-4~),
Replaces:
 polkitd (<< 122-4~),
Description: polkit Authorization API - development files
 polkit is a toolkit for defining and handling the policy that
 allows unprivileged processes to speak to privileged processes.
 It was previously named PolicyKit.
 .
 This package contains the development files for the library found in
 libpolkit-gobject-1-0.

Package: libpolkit-agent-1-0
Architecture: any
Section: libs
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Multi-Arch: same
Description: polkit Authentication Agent API
 polkit is a toolkit for defining and handling the policy that
 allows unprivileged processes to speak to privileged processes.
 It was previously named PolicyKit.
 .
 This package contains a library for accessing the authentication agent.

Package: libpolkit-agent-1-dev
Architecture: any
Section: libdevel
Depends:
 gir1.2-polkit-1.0 (= ${binary:Version}),
 libpolkit-agent-1-0 (= ${binary:Version}),
 libpolkit-gobject-1-dev,
 ${gir:Depends},
 ${misc:Depends},
Provides:
 ${gir:Provides},
Description: polkit Authentication Agent API - development files
 polkit is a toolkit for defining and handling the policy that
 allows unprivileged processes to speak to privileged processes.
 It was previously named PolicyKit.
 .
 This package contains the development files for the library found in
 libpolkit-agent-1-0.

Package: gir1.2-polkit-1.0
Section: introspection
Architecture: any
Depends:
 ${gir:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Provides:
 gir1.2-polkitagent-1.0 (= ${binary:Version}),
 ${gir:Provides},
Description: GObject introspection data for polkit
 polkit is a toolkit for defining and handling the policy that
 allows unprivileged processes to speak to privileged processes.
 It was previously named PolicyKit.
 .
 This package contains introspection data for polkit.
 .
 It can be used by packages using the GIRepository format to generate
 dynamic bindings.
